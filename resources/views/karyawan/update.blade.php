@extends('layouts.main')

@section('container')
    <div class="wrapper">
        <!-- Navbar -->
        @include('partials.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('partials.sidebar')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Update Karyawan</h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- jquery validation -->
                            <div class="card card-primary">
                                @if (session()->has('error'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ session('error') }}
                                    </div>
                                @endif
                                @if (session()->has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <form id="quickForm" action="{{ route('karyawan.update.post', ['id' => $employe->id]) }}"
                                    method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="name">posisi jabatan</label>
                                            <select name="posisi_jabatan" id="posisi_jabatan"
                                                class="form-control @error('posisi_jabatan')
                                                is-invalid
                                            @enderror">
                                                <option value="">Select Posisi Jabatan</option>
                                                @foreach ($positions as $key => $position)
                                                    <option @selected(old('posisi_jabatan', $employe->position_id) == $position->id) value="{{ $position->id }}">
                                                        {{ $position->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('posisi_jabatan')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="name">name karyawan</label>
                                            <input type="text" name="name"
                                                class="form-control @error('name')
                                                is-invalid
                                            @enderror"
                                                id="name" placeholder="Masukan Nama karyawan"
                                                value="{{ old('name', $employe->name) }}">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="nik">nik karyawan</label>
                                            <input type="text" name="nik"
                                                class="form-control @error('nik')
                                                is-invalid
                                            @enderror"
                                                id="nik" placeholder="Masukan nik karyawan"
                                                value="{{ old('nik', $employe->nik) }}">
                                            @error('nik')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="gender">gender karyawan</label>
                                            <div class="form-check">
                                                <input class="form-check-input" value="male" type="radio" name="gender"
                                                    {{ old('gender', $employe->gender) == 'male' ? 'checked' : '' }}>
                                                <label class="form-check-label">male</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" value="female" type="radio" name="gender"
                                                    {{ old('gender', $employe->gender) == 'female' ? 'checked' : '' }}>
                                                <label class="form-check-label">female</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">status karyawan</label>
                                            <div class="form-check">
                                                <input class="form-check-input" value="Karyawan Tetap" type="radio"
                                                    name="status"
                                                    {{ old('status', $employe->status) == 'Karyawan Tetap' ? 'checked' : '' }}>
                                                <label class="form-check-label">Karyawan Tetap</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" value="Karyawan Tidak Tetap" type="radio"
                                                    name="status"
                                                    {{ old('status', $employe->status) == 'Karyawan Tidak Tetap' ? 'checked' : '' }}>
                                                <label class="form-check-label">Karyawan Tidak Tetap</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">image karyawan</label>
                                            </br>
                                            <input type="file"
                                                class="@error('image')
                                                is-invalid
                                            @enderror"
                                                name="image" id="image">
                                            @error('image')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                        <!-- right column -->
                        <div class="col-md-6">

                        </div>
                        <!--/.col (right) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
@endsection

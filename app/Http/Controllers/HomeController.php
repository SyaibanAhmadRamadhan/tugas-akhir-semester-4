<?php

namespace App\Http\Controllers;

use App\Models\Employes;
use App\Models\Position;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index', [
            'title' => 'home',
            'position' => Position::all(),
            'employe' => Employes::all()
        ]);
    }
}

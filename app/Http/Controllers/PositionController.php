<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function index()
    {
        return view('position.index', [
            'title' => 'position',
            "positions" => Position::all()
        ]);
    }

    public function create()
    {
        return view('position.create', [
            'title' => 'position'
        ]);
    }

    public function store(Request $request)
    {
        $validasi = $request->validate([
            'name' => 'required|max:100|unique:positions,name',
            "basic_salary" => "required",
        ]);
        $basicSalaryInt = preg_replace('/[^0-9]/', '', $request->basic_salary);

        Position::create([
            "name" => $validasi["name"],
            "basic_salary" => $basicSalaryInt,
        ]);

        return redirect()->back()->with("success", "created position successfuly");
    }

    public function show($id)
    {

        $position = Position::where('id', $id)->first();
        if ($position == null) {
            abort(404);
        }
        return view('position.update', [
            'title' => 'position',
            "position" => $position
        ]);
    }

    public function update(Request $request, $id)
    {
        $validasi = $request->validate([
            'name' => 'required|max:100|unique:positions,name,' . $id,
            "basic_salary" => "required",
        ]);
        $basicSalaryInt = preg_replace('/[^0-9]/', '', $request->basic_salary);

        Position::where('id', $id)->update([
            "name" => $validasi["name"],
            "basic_salary" => $basicSalaryInt,
        ]);

        return redirect()->back()->with("success", "update position successfuly");
    }

    public function delete($id)
    {
        $position = Position::where('id', $id)->first();
        if ($position == null) {
            abort(404);
        }
        $position->delete();

        return redirect()->back()->with("success", "delete position successfuly");
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Employes;
use App\Models\Position;
use Illuminate\Http\Request;

class EmployeController extends Controller
{
    public function index()
    {
        return view('karyawan.index', [
            'title' => 'karyawan',
            'employes' => Employes::all()
        ]);
    }

    public function create()
    {
        return view('karyawan.create', [
            'title' => 'karyawan',
            "positions" => Position::all()
        ]);
    }

    public function store(Request $request)
    {
        $validasi = $request->validate([
            'posisi_jabatan' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'status' => 'required',
            'nik' => 'required|max:100|unique:employes,nik',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $imageName = time() . '.' . $request->image->extension();
        $request->image->move(public_path('images'), $imageName);
        Employes::create([
            "position_id" => $validasi["posisi_jabatan"],
            "nik" => $validasi["nik"],
            "name" => $validasi["name"],
            "gender" => $validasi["gender"],
            "status" => $validasi["status"],
            "image" => $imageName,
        ]);

        return redirect()->back()->with("success", "created karyawan successfuly");
    }

    public function show($id)
    {

        $employe = Employes::where('id', $id)->first();
        if ($employe == null) {
            abort(404);
        }
        return view('karyawan.update', [
            'title' => 'karyawan',
            "employe" => $employe,
            "positions" => Position::all()
        ]);
    }

    public function update(Request $request, $id)
    {
        $validasi = $request->validate([
            'posisi_jabatan' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'status' => 'required',
            'nik' => 'required|max:100|unique:employes,nik,' . $id,
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        $employe = Employes::where('id', $id)->first();
        if ($request->image) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            Employes::where('id', $id)->update([
                "position_id" => $validasi["posisi_jabatan"],
                "nik" => $validasi["nik"],
                "name" => $validasi["name"],
                "gender" => $validasi["gender"],
                "status" => $validasi["status"],
                "image" => $imageName,
            ]);
            unlink("images/" . $employe->image);
        } else {
            Employes::where('id', $id)->update([
                "position_id" => $validasi["posisi_jabatan"],
                "nik" => $validasi["nik"],
                "name" => $validasi["name"],
                "gender" => $validasi["gender"],
                "status" => $validasi["status"],
            ]);
        }


        return redirect()->back()->with("success", "updated karyawan successfuly");
    }

    public function delete($id)
    {
        $employe = Employes::where('id', $id)->first();
        if ($employe == null) {
            abort(404);
        }
        unlink("images/" . $employe->image);
        $employe->delete();

        return redirect()->back()->with("success", "delete karyawan successfuly");
    }
}

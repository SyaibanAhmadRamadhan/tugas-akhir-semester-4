<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employes extends Model
{
    use HasFactory;
    protected $table = 'employes';
    protected $guarded = ["id"];

    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }
}

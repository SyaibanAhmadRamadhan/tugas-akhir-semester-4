<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmployeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\SlipGajiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/', 'middleware' => 'auth'], function () {
    Route::get("/", [HomeController::class, "index"])->name('home');
    Route::get("/karyawan", [EmployeController::class, "index"])->name('karyawan.index');
    Route::get("/karyawan/create", [EmployeController::class, "create"])->name('karyawan.create');
    Route::post("/karyawan/create", [EmployeController::class, "store"])->name('karyawan.create.post');
    Route::get("/karyawan/update/{id}", [EmployeController::class, "show"])->name('karyawan.update');
    Route::put("/karyawan/update/{id}", [EmployeController::class, "update"])->name('karyawan.update.post');
    Route::delete("/karyawan/delete/{id}", [EmployeController::class, "delete"])->name('karyawan.delete.post');

    Route::get("/position", [PositionController::class, "index"])->name('position.index');
    Route::get("/position/create", [PositionController::class, "create"])->name('position.create');
    Route::post("/position/create", [PositionController::class, "store"])->name('position.create.post');
    Route::get("/position/update/{id}", [PositionController::class, "show"])->name('position.update');
    Route::put("/position/update/{id}", [PositionController::class, "update"])->name('position.update.post');
    Route::delete("/position/delete/{id}", [PositionController::class, "delete"])->name('position.delete.post');

    Route::get("/slip-gaji", [SlipGajiController::class, "index"])->name('slipgaji.index');
    Route::get("/slip-gaji/cetak", [SlipGajiController::class, "cetak"])->name('slipgaji.cetak');
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::group(['prefix' => '/auth', 'middleware' => 'guest'], function () {
    Route::get("/login", [AuthController::class, "index"])->name('auth.login');
    Route::post("/login", [AuthController::class, "login"])->name('auth.login.post');
});

// Route::get('/', function () {
//     return view('welcome');
// });
